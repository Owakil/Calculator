<?php

namespace Pondit\Displayer;

class Displayer
{
    function displaysimple($story)
    {
        //print add result
        echo $story;
    }

    function displayH1($story)
    {
        echo "<h1>";
        echo $story;
        echo "</h1>";
    }

    function displaypre($story)
    {
        //echo "<hr />";
        echo "<pre>";
        echo $story;
        echo "</pre>";
        echo "<hr />";
    }
}