<?php

namespace Pondit\Request;


class Request
{
    public $get = [];
    public $post = [];
    public $request = [];

    public function __construct($request)
    {


        $this->request = $request;

    }

    public function getNumber($name)
    {
        if ($this->has($name) && $this->isNotEmpty($name)) {
            return $this->request[$name];
        }
        //return $this->request[$name];
        return 0;
    }

    public function has($name)
    {
        if (array_key_exists($name, $this->request)) {
            //$number1=$_GET['number1'];
            return true;
        }
        return false;
    }

    public function isEmpty($value)
    {
        return empty($value);
    }

    //public function isNotEmpty($value)
    public function isNotEmpty($name)
    {
        return !empty($this->request[$name]);
    }
}