<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\NumbersCalculator\Subtraction;
use Pondit\Displayer\Displayer;
use Pondit\Request\Request;

$request = new Request($_REQUEST);
$subtraction = new Subtraction();
$subtraction->number1 = $request->getNumber('number1');
$subtraction->number2 = $request->getNumber('number2');
$result = $subtraction->sub2numbers();
echo $result;
$displayer = new Displayer();
$displayer = new Displayer();
$displayer->displayH1($result);
$displayer->displaypre($result);
$displayer->displaypre('visible');
