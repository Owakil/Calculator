<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\NumbersCalculator\Division;
use Pondit\Displayer\Displayer;
use Pondit\Request\Request;

$request = new Request($_REQUEST);
$division = new Division();
$division->number1 = $request->getNumber('number1');
$division->number2 = $request->getNumber('number2');
$result = $division->div2numbers();
echo $result;
$displayer = new Displayer();
$displayer = new Displayer();
$displayer->displayH1($result);
$displayer->displaypre($result);
$displayer->displaypre('annouce');