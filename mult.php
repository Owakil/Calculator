<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\NumbersCalculator\Multiplication;
use Pondit\Displayer\Displayer;
use Pondit\Request\Request;

$request = new Request($_REQUEST);
$multiplication = new Multiplication();
$multiplication->number1 = $request->getNumber('number1');
$multiplication->number2 = $request->getNumber('number2');
$result = $multiplication->mult2numbers();
echo $result;
$displayer = new Displayer();
$displayer = new Displayer();
$displayer->displayH1($result);
$displayer->displaypre($result);
$displayer->displaypre('productivity');
